package ee.bcs.valiit;

public class StandardV2ljundisse {

    public static void main(String[] args) {
        System.out.println("Hello, World!");
        System.out.println("Hello \"World\"!");
        System.out.println("Steven Hawking once said:\"Life would be tragic if it weren't funny\"");
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\"");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rogkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on:\"Sõida tasai üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli', \"viis\" - viis.");


    }

}
