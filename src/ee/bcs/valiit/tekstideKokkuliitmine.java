package ee.bcs.valiit;

public class tekstideKokkuliitmine {

    // Ülesanne 1.
    /*
    public static void main(String[] args) {
        String tallinnPopulation = "450 000";
        String[] numArray = tallinnPopulation.split(" ");
        StringBuffer stringBuffer = new StringBuffer();
        for (String num : numArray) {
            stringBuffer.append(num);
        }
        Integer populationOfTallinn = Integer.valueOf(stringBuffer.toString());
        StringBuilder stringBuilder = new StringBuilder("Tallinnas elab ").append(tallinnPopulation).append(" inimest");
        System.out.println(stringBuilder);
        stringBuilder.setLength(0);
        stringBuilder.append("Tallinnas elab ")
                .append(populationOfTallinn)
                .append(" inimest");
        System.out.println(stringBuilder); */

        // ÜLesanne 2

        public static void main(String[] args) {

            String bookTitle = "Rehepapp";

            //1.

            String resultaat = "Raamatu \"" + bookTitle + "\" autor on\n Andrus Kivirähk "; // \n - reavahetus.

            //2.

            StringBuilder resultaat2 = new StringBuilder();

            resultaat2.append("Raamatu ");
            resultaat2.append('"');
            resultaat2.append(bookTitle);
            resultaat2.append('"');
            resultaat2.append(" autor on Andrus Kivirähk");

            //3.

            String resultaat3 = String.format("Raamatu %c%s%c autor on Andrus Kivirähk", '"', bookTitle, '"');


            System.out.println(resultaat);
            System.out.println(resultaat2);
            System.out.println(resultaat3);


    }
}
